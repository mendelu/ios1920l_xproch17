//
//  DetailViewController.swift
//  cv07_CoreData
//
//  Created by David on 09/04/2020.
//  Copyright © 2020 David Prochazka. All rights reserved.
//

import UIKit

class NewPersonViewController: UIViewController {

    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var surnameTF: UITextField!
    @IBOutlet weak var ageTF: UITextField!
    
    var personStore: PersonStoreDelegate? = nil
    
    func configureView() {
        // Update the user interface for the detail item.
        /*if let detail = detailItem {
            if let label = detailDescriptionLabel {
                label.text = detail.name
            }
        }
        */
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureView()
    }

    @IBAction func saveClicked(_ sender: Any) {
        let name = nameTF.text ?? "Name not known"
        let surname = surnameTF.text ?? "Surname not known"
        let age = ageTF.text ?? "Age not known"
        let ageFloat = Float(age) ?? 0.0
        
        if let store = personStore{
            store.insertPerson(name: name, surname: surname, age: ageFloat)
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func cancelClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}


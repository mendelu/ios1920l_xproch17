//
//  PersonStoreDelegate.swift
//  cv07_CoreData
//
//  Created by David on 09/04/2020.
//  Copyright © 2020 David Prochazka. All rights reserved.
//

import Foundation

protocol PersonStoreDelegate {
    func insertPerson(name: String, surname: String, age: Float)
}

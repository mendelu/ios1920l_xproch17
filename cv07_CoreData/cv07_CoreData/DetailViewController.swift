//
//  DetailViewController.swift
//  cv07_CoreData
//
//  Created by David on 09/04/2020.
//  Copyright © 2020 David Prochazka. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var detailDescriptionLabel: UILabel!


    func configureView() {
        // Update the user interface for the detail item.
        if let detail = detailItem {
            if let label = detailDescriptionLabel {
                label.text = detail.name
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureView()
    }

    var detailItem: Person? {
        didSet {
            // Update the view.
            configureView()
        }
    }


}


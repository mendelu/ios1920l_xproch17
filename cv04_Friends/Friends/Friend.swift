//
//  Friend.swift
//  Friends
//
//  Created by David on 19/03/2020.
//  Copyright © 2020 David Prochazka. All rights reserved.
//

import Foundation

class Friend{
    var name: String = "No name"
    var address: String = "No address"
    
    init(name: String, address: String) {
        self.name = name
        self.address = address
    }
}

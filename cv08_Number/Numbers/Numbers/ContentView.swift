//
//  ContentView.swift
//  Numbers
//
//  Created by David on 16/04/2020.
//  Copyright © 2020 David Prochazka. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @State var score: Int = 0
    @State var guessedNumber: Int = Int.random(in: 1...100)
    @State var sliderValue: Double = 0.0
    var sliderValueInt: Int{
        Int(self.sliderValue.rounded())
    }

    var body: some View {
        VStack(alignment: .center, spacing: 20.0) {
            
            HStack(spacing: 10.0){
                Text("Guess number: ")
                Text("\(self.guessedNumber)")
            }
            
            HStack(spacing: 5.0){
                Text("0")
                Slider(value: self.$sliderValue)
                Text("100")
            }
            
            Button(action: {
                self.score += self.calculatePoints()
                self.guessedNumber = Int.random(in: 1...100)
            }) {
                Text("Guess")
            }
            
            HStack(spacing: 10.0){
                Text("Score: ")
                Text("\(self.score)")
            }
            
        }
        .padding(.all, 10.0)
        .onAppear(){
            // zavola se pokud se zobrazi komponenta
        }
    }
    
    func calculatePoints() -> Int {
        return 100-abs(self.guessedNumber-self.sliderValueInt)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

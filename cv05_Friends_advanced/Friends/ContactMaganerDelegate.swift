//
//  ContactMaganerDelegate.swift
//  Friends
//
//  Created by David on 26/03/2020.
//  Copyright © 2020 David Prochazka. All rights reserved.
//

import Foundation

protocol ContactManagerDelegate {
    func saveContact(name: String, address: String)
}

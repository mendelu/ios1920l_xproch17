//
//  NewViewController.swift
//  Friends
//
//  Created by David on 26/03/2020.
//  Copyright © 2020 David Prochazka. All rights reserved.
//

import UIKit

class NewViewController: UIViewController {

    var saveDelegate: ContactManagerDelegate?
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var addressTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

    }
    
    @IBAction func cancelClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveClicked(_ sender: Any) {
        if let save = saveDelegate{
            let name = nameTextField.text ?? "No name"
            let addr = addressTextField.text ?? "No address"
            save.saveContact(name: name, address: addr)
        }
        
        dismiss(animated: true, completion: nil)
    }
}

//
//  EditViewController.swift
//  Friends
//
//  Created by David on 26/03/2020.
//  Copyright © 2020 David Prochazka. All rights reserved.
//

import UIKit

class EditViewController: UIViewController {

    var saveDelegate: ContactManagerDelegate?
    var editedFriend: Friend?{
        didSet {
            // Update the view.
            configureView()
        }
    }
    
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var addressTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureView()
    }
    
    
    @IBAction func cancel2Clicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveClicked(_ sender: Any) {
        if let save = saveDelegate{
            let name = nameTextField.text ?? "No name"
            let addr = addressTextField.text ?? "No address"
            save.saveContact(name: name, address: addr)
        }
        dismiss(animated: true, completion: nil)
    }
    
    func configureView() {
        // Update the user interface for the detail item.
        if let friend = editedFriend {
            if let name = nameTextField {
                name.text = friend.name
            }
            if let addr = addressTextField {
                addr.text = friend.address
            }
        }
    }
    
}

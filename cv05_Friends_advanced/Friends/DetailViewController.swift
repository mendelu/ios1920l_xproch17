//
//  DetailViewController.swift
//  Friends
//
//  Created by David on 19/03/2020.
//  Copyright © 2020 David Prochazka. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    
    var selectedFriend: Friend? {
        didSet {
            // Update the view.
            configureView()
        }
    }
    
    
    func configureView() {
        // Update the user interface for the detail item.
        if let friend = selectedFriend {
            if let name = nameLabel {
                name.text = friend.name
            }
            if let addr = addressLabel {
                addr.text = friend.address
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureView()
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "editContact" {
            let controller = (segue.destination as! UINavigationController).topViewController as! EditViewController
            if let friend = selectedFriend{
                controller.editedFriend = friend
            }
        }
        
    }
    
}


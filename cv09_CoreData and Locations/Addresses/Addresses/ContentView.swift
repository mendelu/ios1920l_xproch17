//
//  ContentView.swift
//  Addresses
//
//  Created by David on 23/04/2020.
//  Copyright © 2020 David Prochazka. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @Environment(\.managedObjectContext) var viewContext
    @State var newContactPresented: Bool = false
    
    
    var body: some View {
        NavigationView{
            ContactList()
            .navigationBarTitle("Contacts")
            .navigationBarItems(
                trailing: Button(
                    action: {
                        self.newContactPresented = true
                        //withAnimation { Contact.create(context: self.viewContext, name: "David", address: "Brno") }
                    }
                ) {
                    Image(systemName: "plus")
                }.sheet(isPresented: $newContactPresented){
                    Text("New Contact Window")
                }
            )
        }
    }
}

struct ContactList: View {
    @FetchRequest(
        sortDescriptors: [NSSortDescriptor(keyPath: \Contact.name, ascending: true)],
        animation: .default)
    var contactList: FetchedResults<Contact>
    
    var body: some View {
        List{
            ForEach(contactList, id: \.self) { contact in
                Text("\(contact.name ?? "No name"), \(contact.address ?? "No name")")
            }
        }
    }
}

/*
struct TableCell: View {
    var body: some View{
        
    }
}
*/

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

//
//  Contact+Extension.swift
//  Addresses
//
//  Created by David on 23/04/2020.
//  Copyright © 2020 David Prochazka. All rights reserved.
//

import SwiftUI
import CoreData

extension Contact: Identifiable{
    
    static func create(context: NSManagedObjectContext, name: String, address: String){
        
        let newContact = Contact(context: context)
        newContact.id = UUID()
        newContact.name = name
        newContact.address = address

        do{
            try context.save()
        } catch {
            // korektni osetreni chyby
            print(error)
        }
    }
    
}


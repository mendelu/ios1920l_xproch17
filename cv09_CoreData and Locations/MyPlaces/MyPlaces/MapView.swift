//
//  MapView.swift
//  MyPlaces
//
//  Created by David on 23/04/2020.
//  Copyright © 2020 David Prochazka. All rights reserved.
//

import SwiftUI
import MapKit

struct MapView: UIViewRepresentable {
    var locationManager = CLLocationManager()
    @Binding var points: [LocationPin]
    
    func startTracking() {
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        //locationManager.requestAlwaysAuthorization()
    }
    
    func makeUIView(context: Context) -> MKMapView {
        startTracking()
        
        let mapView = MKMapView(frame: UIScreen.main.bounds)
        mapView.showsUserLocation = true
        mapView.userTrackingMode = .follow
        mapView.addAnnotations(points)
        return mapView
    }
    
    func updateUIView(_ uiView: MKMapView, context: Context) {
    }
}


/*
struct MapView: View {
    var body: some View {
        Text("Hello, World!")
    }
}
*/

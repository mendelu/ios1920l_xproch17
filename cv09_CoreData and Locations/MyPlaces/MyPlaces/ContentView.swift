//
//  ContentView.swift
//  MyPlaces
//
//  Created by David on 23/04/2020.
//  Copyright © 2020 David Prochazka. All rights reserved.
//

import SwiftUI
import MapKit

class LocationPin: NSObject, MKAnnotation{
    let coordinate: CLLocationCoordinate2D
    let title: String?
    
    init(coordinate: CLLocationCoordinate2D, title: String?) {
        self.coordinate = coordinate
        self.title = title
    }
}

struct ContentView: View {
    @State var mapPoints: [LocationPin] = [
        LocationPin(coordinate: .init(latitude: 51.510186, longitude: -0.123576), title: "London"),
        LocationPin(coordinate: .init(latitude: 51.710186, longitude: -0.233576), title: "London 2 ")
    ]

    var body: some View {
        MapView(points: $mapPoints)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
